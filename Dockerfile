#FROM hypriot/rpi-python  : for arm platform  #   https://github.com/hypriot/rpi-python
FROM python:2.7
USER root
COPY . /data
WORKDIR /data
#CMD ["python", "./main.py"]
RUN chmod +x /data/*.py  
CMD python ./main.py